# $installedapps = get-appxpackage

# $aumidlist = @()
# foreach ($app in $installedapps)
# {
	# foreach ($id in (get-appxpackagemanifest $app).package.applications.application.id)
	# {
		# $aumidlist += $app.packagefamilyname + "!" + $id
	# }
# }

# $aumidlist

get-appxpackage *3dbuilder* | remove-appxpackage
get-appxpackage *getstarted* | remove-appxpackage
get-appxpackage *windowscamera* | remove-appxpackage
get-appxpackage *windowsmaps* | remove-appxpackage
get-appxpackage *windowscalculator* | remove-appxpackage
get-appxpackage *bingfinance* | remove-appxpackage

# Get Skype
get-appxpackage *skypeapp* | remove-appxpackage

# Get Office
get-appxpackage *officehub* | remove-appxpackage
get-appxpackage *windowsphone* | remove-appxpackage
get-appxpackage *zunemusic* | remove-appxpackage
get-appxpackage *photos* | remove-appxpackage
get-appxpackage *solitairecollection* | remove-appxpackage
get-appxpackage *onenote* | remove-appxpackage
get-appxpackage *people* | remove-appxpackage
get-appxpackage *windowsalarms* | remove-appxpackage

# Calendar and Mail
get-appxpackage *windowscommunicationsapps* | remove-appxpackage
get-appxpackage *zunevideo* | remove-appxpackage
get-appxpackage *bingnews* | remove-appxpackage
get-appxpackage *windowsstore* | remove-appxpackage
get-appxpackage *bingsports* | remove-appxpackage
get-appxpackage *soundrecorder* | remove-appxpackage
get-appxpackage *bingweather* | remove-appxpackage
get-appxpackage *xboxapp* | remove-appxpackage
get-appxpackage *mspaint* | remove-appxpackage
get-appxpackage *messaging* | remove-appxpackage
get-appxpackage *wallet* | remove-appxpackage